import subprocess
import os

manual = []

# Set path to current directory
path = os.getcwd()
sums_path = path + '/human_sums.txt'


def check_prior(file):
	prior = str(subprocess.check_output([f'find . -name {file}'], shell=True))
	if file in prior:
		return True
	else:
		return False


def reference_check(i):
	with open(sums_path, 'r') as f:
		if i in f.read():
			print('File successfully validated: ' + str(i).rsplit(' ', 1)[1])
			return True
		else:
			print('File failed validation: ' + str(i).rsplit(' ', 1)[1])
			return False


def download_reference(cmd, alt=None):
	if alt == None:
		file = cmd.rsplit('/', 1)[1]
	else:
		file = alt
	attempt = 0
#   while True:
#		if attempt == 3:
#			manual.append(f'{file}')
#			return False
	subprocess.run([f'{cmd}'], shell=True)
#		check = str(subprocess.check_output([f'md5sum {file}'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#		if reference_check(check):
	return True
#		else:
#			subprocess.run([f'rm {file}'], shell=True)
#			attempt += 1


### Genomic reference ###

subprocess.run([f'mkdir -p {path}/references/homo_sapiens'], shell=True)
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('Homo_sapiens.assembly38.no_ebv.fa'):
	os.chdir(f'{path}/references/homo_sapiens')
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/fasta'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/fasta')

	download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: Homo_sapiens.assembly38.no_ebv.fa')


### Show list of files that could not be validated ###

if len(manual) > 0:
	print('These files could not be validated and must be downloaded manually. Please refer to the LENS documentation.')
	for item in manual:
		print(item)
else:
	print('All human references downloaded successfully!')
